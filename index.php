<?php include("inc/header.php"); ?>

<div class="page_homepage">

    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- slider -->
    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->

    <div class="slider">

        <img src="img/slide1.jpg" alt="" />
        
        <div class="slider__text">
            <p>Tesque a felis venenatis varius, mauris in quam ac magna</p>
        </div>  <!-- /slider__text -->
        
        <ul class="slider__price">
            <li class="slider__price__value">1890,-</li>
            <li class="slider__price__link"><a href="#">Více zde</a></li>
        </ul>   <!-- /slider__price -->

        <ul class="slider__nav">
            <li class="slider__nav__item_prev">Předchozí</li>
            <li class="slider__nav__item_next">Následující</li>
        </ul>   <!-- /slider__nav -->

    </div>  <!-- /slider -->
    <hr class="hide" />

    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- obsah titulni stranky -->
    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->

    <div class="page_homepage__content">

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- boxiky v horni casti -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->

        <ul class="hp_boxes_first">
            <li class="hp_boxes__item hp_boxes__item_1">
                <h2 class="hp_boxes__title">
                    <a href="#" class="hp_boxes__link">Kuchyně</a>
                </h2>
            </li>
            <li class="hp_boxes__item hp_boxes__item_2">
                <h2 class="hp_boxes__title">
                    <a href="#" class="hp_boxes__link">Víno </a>
                </h2>
            </li>
            <li class="hp_boxes__item hp_boxes__item_3">
                <h2 class="hp_boxes__title">
                    <a href="#" class="hp_boxes__link">Restaurace</a>
                </h2>
            </li>
            <li class="hp_boxes__item hp_boxes__item_4 hp_boxes__item_last">
                <h2 class="hp_boxes__title">
                    <a href="#" class="hp_boxes__link">Káva</a>
                </h2>
            </li>
        </ul>   <!-- /hp_boxes -->
        <hr class="hide" />

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- seznam zbozi na titulni strance -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->

        <div class="commodity_list">
            
            <h2 class="commodity_list__title">Vybíráme z nabídky</h2>

            <div class="commodity">
                <h2 class="commodity__title">
                    <a href="#" class="commodity__title__link">Koktejlové sklo Crescendo<br />set 4 ks</a>
                </h2>
                <div class="commodity__pic">
                    <img src="img/commodity/pic1.jpg" />
                </div>
                <ul class="commodity__price">
                    <li class="commodity__price__item_old">360 Kč</li>
                    <li class="commodity__price__item_current">360 Kč</li>
                </ul>
            </div>  <!-- /commodity -->

            <div class="commodity">
                <h2 class="commodity__title">
                    <a href="#" class="commodity__title__link">Koktejlové sklo Crescendo<br />set 4 ks</a>
                </h2>
                <div class="commodity__pic">
                    <img src="img/commodity/pic1.jpg" />
                </div>
                <ul class="commodity__price">
                    <li class="commodity__price__item_old">360 Kč</li>
                    <li class="commodity__price__item_current">360 Kč</li>
                </ul>
            </div>  <!-- /commodity -->

            <div class="commodity commodity_last">
                <h2 class="commodity__title">
                    <a href="#" class="commodity__title__link">Koktejlové sklo Crescendo<br />set 4 ks</a>
                </h2>
                <div class="commodity__pic">
                    <img src="img/commodity/pic1.jpg" />
                </div>
                <ul class="commodity__price">
                    <li class="commodity__price__item_old">360 Kč</li>
                    <li class="commodity__price__item_current">360 Kč</li>
                </ul>
            </div>  <!-- /commodity -->

            <ul class="commodity_list__navigation">
                <li class="commodity_list__navigation__item_prev">&lt;</li>
                <li class="commodity_list__navigation__item_next">&gt;</li>
            </ul>
        </div>

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- obsahove boxiky -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->

        <div class="hp_content_area">
            
            <div class="hp_content_area__left">
                <div class="hp_content_area__box hp_box_why">
                    <h3 class="hp_content_area__title">Proč nakupovat u nás</h3>
                    <ul>
                        <li><span>1</span> Velký výběr za nejlepší ceny</li>
                        <li><span>2</span> 95% zboží máme skladem</li>
                        <li><span>3</span> 14-ti denní záruka vrácení zboží</li>
                    </ul>
                </div>  <!-- /hp_content_area__box -->

                <div class="hp_content_area__box hp_box_contact ">
                    <h3 class="hp_content_area__title">Kontaktujte nás</h3>
                    <ul>
                        <li class="hp_box_content__email"><a href="mailto:info@stolnisklo.cz">info@stolnisklo.cz</a></li>
                        <li class="hp_box_content__phone">+420 603 235 026</li>
                    </ul>
                </div>  <!-- /hp_content_area__box -->
            </div>  <!-- /hp_content_area__left -->

            <div class="hp_content_area__right">
                <div class="hp_content_area__box hp_box_list">
                    <h2 class="hp_content_area__title">Aktuality</h2>

                    <div class="hp_box_list__item">
                        <h4 class="hp_box_list__date">21.2.2013</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vestibulum purus malesuada nisi imperdiet dignissim. Aliquam vel lorem mi, eget ultrices mi.</p>
                    </div>

                    <div class="hp_box_list__item">
                        <h4 class="hp_box_list__date">21.2.2013</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur <a href="#">adipiscing</a> elit. Mauris vestibulum purus malesuada nisi imperdiet dignissim. Aliquam vel lorem mi, eget ultrices mi.</p>
                    </div>

                    <div class="hp_box_list__item">
                        <h4 class="hp_box_list__date">21.2.2013</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vestibulum purus malesuada nisi imperdiet dignissim. Aliquam vel lorem mi, eget ultrices mi.</p>
                    </div>
                </div>
            </div>  <!-- /hp_content_area__right -->

        </div>  <!-- /hp_content_area -->
 
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- reklamni banner -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->

        <div class="advert_area">
            <img src="img/advert1.png" />
        </div>

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- boxiky v dolni casti -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->

        <ul class="hp_boxes_second">
            <li class="hp_boxes__item hp_boxes__item_1">
                <h2 class="hp_boxes__title">
                    <a href="#" class="hp_boxes__link">Skvělá nabídka flakónů</a>
                </h2>
            </li>
            <li class="hp_boxes__item hp_boxes__item_2 hp_boxes__catalog">
                <h2 class="hp_boxes__title">
                    <a href="#" class="hp_boxes__link">Stáhněte si produktový</a>
                </h2>
                <a href="#" class="hp_boxes__download">Katalog v PDF</a>
            </li>
            <li class="hp_boxes__item hp_boxes__item_3 hp_boxes__item_last">
                <h2 class="hp_boxes__title">
                    <a href="#" class="hp_boxes__link">O značce Luigi Bormioli</a>
                </h2>
            </li>
        </ul>   <!-- /hp_boxes -->
        <hr class="hide" />

    </div>  <!-- /page_homepage__content -->

</div>  <!-- /page_homepage -->
<hr class="hide" />

<?php include("inc/footer.php"); ?>