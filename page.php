<?php include("inc/header.php"); ?>

<div class="page_normal">

    <?php include("inc/breadcrumbs.php"); ?>

    <div class="page_layout">

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- levy sloupec -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->

        <div class="aside_area">
        <div class="aside">
            <h2 class="aside__title">Filtrovat podle</h2>

            <h3 class="aside__category__title">Kategorie</h3>
            <ul class="aside__list">
                <li class="aside__list__item"><input type="checkbox" /> Sklenice na víno <span>(17)</span></li>
                <li class="aside__list__item"><input type="checkbox" /> Sklenice na koktejly <span>(14)</span></li>
                <li class="aside__list__item"><input type="checkbox" /> Sklenice na sekt <span>(6)</span></li>
            </ul>

            <h3 class="aside__category__title">Barva</h3>
            <ul class="aside__list">
                <li class="aside__list__item"><input type="checkbox" /> Čirá <span>(37)</span></li>
                <li class="aside__list__item"><input type="checkbox" /> Bílá <span>(1)</span></li>
                <li class="aside__list__item"><input type="checkbox" /> Barevná <span>(0)</span></li>
            </ul>

            <h3 class="aside__category__title">Materiálu</h3>
            <ul class="aside__list">
                <li class="aside__list__item"><input type="checkbox" /> Sklo <span>(35)</span></li>
                <li class="aside__list__item"><input type="checkbox" /> Křišťál <span>(2)</span></li>
                <li class="aside__list__item"><input type="checkbox" /> Porcelán <span>(0)</span></li>
            </ul>

        </div>  <!-- /aside -->
        </div>  <!-- /aside_area -->

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- vypis kategorie -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->

        <div class="content_area">
        <div class="category__list">
            <h1 class="category__title">Sklenice se stopkou</h1>

        <div class="category__list__area">
            <div class="category__filtr">
                <h3 class="category__filtr__title">Váš filtr:</h3>
                <ul class="category__filtr__list">
                    <li class="category__filtr__list__item">Čirá</li>
                    <li class="category__filtr__list__item">Sklo</li>
                </ul>
            </div>

            <?php for($i=0; $i<9; $i++){ ?>
            <div class="commodity">
                <h2 class="commodity__title">
                    <a href="#" class="commodity__title__link">Koktejlové sklo Crescendo<br />set 4 ks</a>
                </h2>
                <div class="commodity__pic">
                    <img src="img/commodity/pic1.jpg" />
                </div>
                <ul class="commodity__price">
                    <li class="commodity__price__item_old">360 Kč</li>
                    <li class="commodity__price__item_current">360 Kč</li>
                </ul>
            </div>  <!-- /commodity -->
            <?php } ?>
        </div>  <!-- /category__list__area -->

        <div class="category__buttons">
            <a href="#" class="category__btn"><img src="img/btn_next.png" /></a>
        </div>

        </div>  <!-- /category__list -->
        </div>  <!-- /content_area -->

    </div>  <!-- /page_layout -->

</div>  <!-- /page_normal -->
<hr class="hide" />

<?php include("inc/footer.php"); ?>