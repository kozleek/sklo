<?php include("inc/header.php"); ?>

<div class="page_normal">

    <?php include("inc/breadcrumbs.php"); ?>

    <div class="page_layout">

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- levy sloupec -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->

        <div class="aside_area">
        <div class="aside">
            <h2 class="aside__title">Články</h2>

            <ul class="article__list">
                <li class="article__list__item"><a href="#" class="article__list__link article__list__link_active">Co jste možná nevěděli</a></li>
                <li class="article__list__item"><a href="#" class="article__list__link">Jak nakupovat</a></li>
                <li class="article__list__item"><a href="#" class="article__list__link">Obchodní podmínky</a></li>
                <li class="article__list__item"><a href="#" class="article__list__link">Možnosti platby</a></li>
                <li class="article__list__item"><a href="#" class="article__list__link">Způsoby doručení</a></li>
                <li class="article__list__item"><a href="#" class="article__list__link">Ochrana osobních údajů</a></li>
                <li class="article__list__item"><a href="#" class="article__list__link">Kontakty</a></li>
            </ul>

        </div>  <!-- /aside -->

        <div class="aside_banner">
            <a href="#"><img src="img/banner_1.jpg" /></a>
        </div>

        <div class="aside_banner">
            <a href="#"><img src="img/banner_2.jpg" /></a>
        </div>

        </div>  <!-- /aside_area -->

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- vypis kategorie -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->

        <div class="content_area">
        <div class="article">

            <h1 class="article__title">Co jste možná nevěděli nadpis H1</h1>
            <div class="article__perex">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eleifend leo et massa feugiat sollicitudin. Integer venenatis consequat lacus vitae hendrerit. Vivamus id urna vel velit semper vulputate eget tempus felis. Phasellus vel nisl lorem. Praesent sit amet odio vel leo viverra tempus. Integer sed mi eu nisi eleifend pulvinar. </p>
            </div>

            <h2>Co jste možná nevěděli nadpis H2</h2>
            <p>Lorem ipsum dolor sit amet, <a href="">consectetur adipiscing elit</a>. Praesent eleifend leo et massa feugiat sollicitudin. Integer venenatis consequat lacus vitae hendrerit. Vivamus id urna vel velit semper vulputate eget tempus felis. Phasellus vel nisl lorem. Praesent sit amet odio vel leo viverra tempus. Integer sed mi eu nisi eleifend pulvinar. Fusce placerat massa id nulla viverra venenatis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut id nibh et urna aliquet tincidunt.</p>
            <cite class="float_right">    Zvýrazněný text / citace - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eleifend leo et massa feugiat sollicitudin. Integer venenatis consequat lacus vitae hendrerit.  Vivamus id urna vel velit semper vulputate. Integer venenatis consequat lacus vitae hendrerit. eget tempus felis. Phasellus vel nisl lorem. Praesent sit amet odio vel leo viverra tempus. Integer sed mi eu nisi eleifend pulvinar.</cite>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eleifend leo et massa feugiat sollicitudin. Integer venenatis consequat lacus vitae hendrerit. Vivamus id urna vel velit semper vulputate eget tempus felis. Phasellus vel nisl lorem. Praesent sit amet odio vel leo viverra tempus. Integer sed mi eu nisi eleifend pulvinar. Fusce placerat massa id nulla viverra venenatis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut id nibh et urna aliquet tincidunt.</p>

            <h3>Co jste možná nevěděli nadpis H3</h3>
            <p>Praesent sit amet odio vel leo viverra tempus. Integer sed mi eu nisi eleifend pulvinar. Fusce placerat massa id nulla viverra venenatis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut id nibh et urna aliquet tincidunt. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eleifend leo et massa feugiat sollicitudin. Integer venenatis consequat lacus vitae hendrerit. Vivamus id urna vel velit semper vulputate eget tempus felis. Phasellus vel nisl lorem. Vivamus id urna vel velit semper vulputate eget tempus felis. Phasellus vel nisl lorem. Vivamus id urna vel velit semper vulputate eget tempus felis. Phasellus vel nisl lorem. </p>
            <img src="img/article_picture.jpg" class="float_left" />
            <p>Praesent eleifend leo et massa feugiat sollicitudin. Integer venenatis consequat lacus vitae hendrerit. Vivamus id urna vel velit semper vulputate eget tempus felis. Phasellus vel nisl lorem. Praesent sit amet odio vel leo viverra tempus. Integer sed mi eu nisi eleifend pulvinar. Fusce placerat massa id nulla viverra venenatis. Vivamus id urna vel velit semper vulputate eget tempus felis. Phasellus vel nisl lorem. Vivamus id urna vel velit semper vulputate eget tempus felis.</p>

            <h4>Co jste možná nevěděli nadpis H4</h4>
            <ul>
                <li>Luigi Bormioli Crescendo 9-ounce Cocktail, Set of 4</li>
                <li>Crystalline Glass</li>
                <li>Hand blown from pure lead-free crystal glass for exceptional clarity</li>
                <li>Lightweight and ideal for wine tasting</li>
                <li>Dishwasher-safe</li>
            </ul>

            <ul class="article__actions">
                <li class="article__actions__item_email">
                    <a href="#" class="article__actions__link">Odeslat článek emailem</a>
                </li>
                <li class="article__actions__item_print">
                    <a href="#" class="article__actions__link">Vytisknout stránku</a>
                </li>
            </ul>

        </div>  <!-- /article -->
        </div>  <!-- /content_area -->

    </div>  <!-- /page_layout -->

</div>  <!-- /page_normal -->
<hr class="hide" />

<?php include("inc/footer.php"); ?>