
    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- paticka -->
    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->

    <div class="footer">

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- odkazy v paticce -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->

        <div class="footer_info">
            <h2 class="footer_info__title">Mohlo by vás zajímat</h2>
            <ul class="footer_info__links">
                <li class="footer_info__links__item"><a class="footer_info__links__link" href="#">Jak nakupovat </a></li>
                <li class="footer_info__links__item"><a class="footer_info__links__link" href="#">Obchodní podmínky</a></li>
                <li class="footer_info__links__item"><a class="footer_info__links__link" href="#">Možnosti platby</a></li>
                <li class="footer_info__links__item"><a class="footer_info__links__link" href="#">Způsoby doručení</a></li>
                <li class="footer_info__links__item"><a class="footer_info__links__link" href="#">Ochrana osobních údajů</a></li>
                <li class="footer_info__links__item"><a class="footer_info__links__link" href="#">Kontakty</a></li>
            </ul>
        </div>  <!-- /footer_info -->

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- newsletter -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->

        <div class="footer_newsletter">
            <h2 class="footer_newsletter__title">Zasílání novinek emailem</h2>
            <p class="footer_newsletter__perex">Přejete si být vždy s předstihem informováni<br />o zajímavých slevách, speciálních akcích a<br />novinkách? Stačí zadat Vaši emailovou adresu.</p>

            <form class="footer_newsletter__form">
                <div class="footer_newsletter__text">
                    <input type="email" class="in_text in_email" />
                </div>
                <div class="footer_newsletter__btn">
                    <input type="submit" class="in_btn" value="Přihlásit" />
                </div>
            </form>
        </div>  <!-- /footer_newsletter__form -->

        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- facebook -->
        <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
        <!-- TODO: nasadit facebook profil -->

        <div class="footer_facebook">
            <img src="img/facebook_fake.png" />
        </div>

    </div>  <!-- /footer -->

</div>  <!-- /envelope -->

<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- sekundarni paticka -->
<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->

<div class="footer_web">
    
    <div class="footer_web__copyright">
        <p>&copy; 2013 - <a href="#">StolniSklo.cz</a></p>
    </div>  <!-- /footer_web__copyright -->

    <div class="footer_web__links">
        <p><a href="/">Úvodní stránka</a> | <a href="/sitemap">Mapa webu</a> | <a href="#">Kontakty</a></p>
        <p>Grafický návrh YAMAN</p>
    </div>  <!-- /footer_web__links -->

</div>  <!-- /footer_web -->

<script type="text/javascript">
	head.js( 
        {jquery: "js/fw/jquery.min.js"}, 
        {log: "js/fw/log.js"}, 
        {custom: "js/custom.js"}
    );
</script>
</body>
</html>
