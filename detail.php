<?php include("inc/header.php"); ?>

<div class="page_normal">

    <?php include("inc/breadcrumbs.php"); ?>

    <div class="page_layout">

        <div class="detail">
            <h1 class="detail__title">Sklenice na víno C182k set 4 ks</h1>

            <div class="detail_layout">

                <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <!-- fotogalerie u produktu -->
                <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->

                <div class="detail_gallery">
                    <div class="detail_gallery__large">
                        <div class="detail_badges">
                            <div class="detail_badge">Novinka</div>
                        </div>  <!-- /detail_badges -->
                        <img src="img/detail/large.jpg" />
                    </div>  <!-- /detail_gallery__large -->

                    <div class="detail_gallery_list">
                        <?php for($i=0; $i<5; $i++){ ?>
                        <div class="detail_gallery_list__item">
                            <a href="img/detail/large2.jpg">
                                <img src="img/detail/small.jpg" />
                            </a>
                        </div>
                        <?php } ?>
                    </div>  <!-- /detail_gallery_list -->
                </div>  <!-- /detail_gallery -->

                <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                <!-- informace o vyrobku -->
                <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->

                <div class="detail_info">
                    <ul class="detail_data">
                        <li>
                            <h4>Kód produktu:</h4>
                            <p>5456465465</p>
                        </li>
                        <li>
                            <h4>Dostupnost:</h4>
                            <p><strong class="availability">Skladem</strong></p>
                        </li>
                        <li>
                            <h4>Termín dodání:</h4>
                            <p>21.3.2013</p>
                        </li>
                    </ul>   <!-- /detail_data -->

                    <ul class="detail_data detail_data_2">
                        <li>
                            <h4>Běžná cena:</h4>
                            <p>660 Kč</p>
                        </li>
                        <li>
                            <h4>Sleva:  <span>57 %</span></h4>
                            <p><strong class="discount">350 Kč</strong></p>
                        </li>
                        <li>
                            <h4>Naše cena:</h4>
                            <p><strong class="price">310 Kč</strong></p>
                        </li>
                    </ul>   <!-- /detail_data -->

                    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                    <!-- pocet polozek -->
                    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->

                    <div class="detail_counter">
                        <h5 class="detail_counter__text">Množství</h5>
                        <div class="detail_counter__input">
                            <input type="text" class="in_text" value="1" />
                        </div>
                        <div class="detail_counter__button">
                            <input type="submit" class="in_btn" value="" />
                        </div>
                    </div>  <!-- /detail_counter -->

                    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                    <!-- socialni site -->
                    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->

                    <div class="detail_social">
                    </div>  <!-- /detail_social -->

                    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
                    <!-- akcni tlacitka -->
                    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->

                    <div class="detail_actions">
                        <li class="detail_actions_mail"><a href="#">Odeslat emailem</a></li>
                        <li class="detail_actions_print"><a href="#">Vytisknout stránku</a></li>
                    </div>  <!-- /detail_actions -->

                </div>
            </div>  <!-- /detail_layout -->

            <div class="detail_related">
                <ul class="detail_related_list">
                    <li class="detail_related_list__item active">Popis</li>
                    <li class="detail_related_list__item">Podobné produkty</li>
                    <li class="detail_related_list__item">Co možná nevíte</li>
                </ul>
            </div>  <!-- /detail_related -->

        </div>  <!-- /detail -->
    </div>  <!-- /page_layout -->

</div>  <!-- /page_normal -->
<hr class="hide" />

<?php include("inc/footer.php"); ?>