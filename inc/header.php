<!DOCTYPE html>
<html>
	<head>		
		<meta charset="utf-8">
		<title>Stolní sklo</title>
				
		<meta name="author" content="code: Tomas -kozleek- Musiol (tomas.musiol@gmail.com)">
		<meta name="description" content="page description">

		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="/favicon.png" />
		
		<link rel="stylesheet" href="css/theme.css" />
		<script src="js/fw/head.min.js"></script>
	</head>
<body class="layout_homepage">

<div class="envelope">

    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <!-- hlavicka -->
    <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++ -->

    <div class="header">

        <div class="logo">
            <img src="img/logo.png" alt="Stolní sklo" width="253" height="46" />
        </div>  <!-- /logo -->
       
        <div class="header__search">
            <div class="header__search__text">
                <input type="text" value="Hledání..." class="in_text in_search" />
            </div>
            <div class="header__search__btn">
                <input type="submit" value="Hledat" class="in_btn in_search_btn" />
            </div>      
        </div>  <!-- /header__search -->    

        <ul class="header__menu_user">
            <li class="header__menu_user__item">
                <a href="#" class="header__menu_user__link">Můj účet</a>
            </li>
            <li class="header__menu_user__item header__cart">
                <a href="#" class="header__menu_user__link header__cart__link">Nákupní taška 
                    <span class="header__cart__counter">3</span>
                </a>
            </li>
        </ul>   <!-- /header__menu_user -->

        <ul class="header__menu">
            <li class="header__menu__item"><a href="#" class="header__menu__link">Sklenice se stopkou</a></li>
            <li class="header__menu__item"><a href="#" class="header__menu__link">Sklenice bez stopky</a></li>
            <li class="header__menu__item"><a href="#" class="header__menu__link">Příslušenství</a></li>
            <li class="header__menu__item"><a href="#" class="header__menu__link">Kolekce</a></li>
            <li class="header__menu__item"><a href="#" class="header__menu__link">karafy</a></li>
            <li class="header__menu__item"><a href="#" class="header__menu__link">Flakóny</a></li>
            <li class="header__menu__item"><a href="#" class="header__menu__link">Zobrazit Vše</a></li>
        </ul>   <!-- /header__menu -->

    </div>  <!-- /header -->
    <hr class="hide" />